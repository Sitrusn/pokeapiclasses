package ru.dexsys.root.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.dexsys.root.dto.NameUrlFieldsAbstract;
import java.util.List;

public class GameIndice {

    @JsonProperty("game_index")
    private int gameIndex;
    private List<Version> versions;

    public class Version extends NameUrlFieldsAbstract {
    }
}