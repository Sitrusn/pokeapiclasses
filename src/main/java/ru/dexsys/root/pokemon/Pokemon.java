package ru.dexsys.root.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Pokemon {

    @JsonProperty("base_experience")
    private String baseExperience;
    @JsonProperty("is_default")
    private boolean isDefault;
    @JsonProperty("held_items")
    private String[] heldItems;
    @JsonProperty("location_area_encounters")
    private String locationAreaEncounters;
    private String name;
    private int order;
    private int id;
    private int height;
    private int weight;

    private List<Ability> abilities;
    private List<Form> forms;
    private List<Specie> species;
    private List<Sprite> sprites;
    private List<FullStat> stats;
    private List<FullType> types;
    private List<FullMove> moves;
    @JsonProperty("game_indices")
    private List<GameIndice> gameIndices;
}