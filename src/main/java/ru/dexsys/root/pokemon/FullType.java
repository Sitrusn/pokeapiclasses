package ru.dexsys.root.pokemon;

import java.util.List;

public class FullType {

    private int slot;
    private List<Type> types;

    public class Type {
        private String type;
        private String url;
    }
}