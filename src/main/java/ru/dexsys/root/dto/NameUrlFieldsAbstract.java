package ru.dexsys.root.dto;

public abstract class NameUrlFieldsAbstract {

    private String name = NameUrlENUM.NAME.value;
    private String url = NameUrlENUM.URL.value;

    public enum NameUrlENUM {
        NAME("pokemonistiyPokemon"),
        URL("https://facebook.com/pokemonistiyPokemon");

        private final String value;

        NameUrlENUM(String value) {
            this.value = value;
        }
    }
}