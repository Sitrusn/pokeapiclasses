package ru.dexsys.root.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.dexsys.root.dto.NameUrlFieldsAbstract;
import java.util.List;

public class FullMove {
    private List<Move> moves;
    @JsonProperty("version_group_details")
    private List<VersionGroupDetail> versionGroupDetails;

    public class Move extends NameUrlFieldsAbstract {
    }

    public class VersionGroupDetail {
        @JsonProperty("level_learned_at")
        private int levelLearnedAt;

        @JsonProperty("move_learn_methods")
        private List<MoveLearnMethod> moveLearnMethods;

        @JsonProperty("version_groups")
        private List<VersionGroup> versionGroups;

        public class MoveLearnMethod extends NameUrlFieldsAbstract{
        }

        public class VersionGroup extends NameUrlFieldsAbstract{
        }
    }
}
