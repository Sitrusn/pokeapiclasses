package ru.dexsys.root.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ability {

    @JsonProperty("is_hidden")
    private boolean isHidden;
    private String name;
    private String url;
    private int slot;
}